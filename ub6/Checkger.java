/*
 * Aufgabe 2 a von Übung 6
 * 
 * @author Nicolas Patzelt
 */

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Checkger {

	/**
	 * @param args
	 */
	
	// der Quelltext zum einlesen ist von http://snipplr.com/view/42391/ übernommen
	static HashMap<Integer, String> textEinlesen(String filename){
		File file = new File(filename);
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String inputLine;
			HashMap<Integer, String> words = new HashMap<Integer, String>();
			while (br.ready()) { // br.read() returns false when the Buffer is not ready (end of file).
				StreamTokenizer parse = new StreamTokenizer(br);
				parse.whitespaceChars('/','/');
				parse.whitespaceChars('-','-');
				parse.whitespaceChars('.','.');
				inputLine = br.readLine();
				String[] result = inputLine.split(" ");
				for (int i = 0; i < result.length; i++) {
					words.put(result[i].hashCode(), result[i]);
				}
			}
			br.close(); // dispose of the resources after using them.
			return words;
		} catch (FileNotFoundException e) { // File not found.
			e.printStackTrace();
			// Put something here like an error message.
			System.out.println("The file could not be found.");
		} catch (IOException e) { // Error with transaction.
			e.printStackTrace();
			// Put something here like an error message.
			System.out.println("There was an error reading the file.");
		}
		return null;
	}
	
	static HashMap<Integer, String> wbEinlesen(String filename){
		File file = new File(filename);
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String inputLine;
			HashMap<Integer, String> dict = new HashMap<Integer, String>();
			while (br.ready()) { // br.read() returns false when the Buffer is not ready (end of file).
				inputLine = br.readLine();
				dict.put(inputLine.hashCode(), inputLine);
			}
			br.close(); // dispose of the resources after using them.
			return dict;
		} catch (FileNotFoundException e) { // File not found.
			e.printStackTrace();
			// Put something here like an error message.
			System.out.println("The file could not be found.");
		} catch (IOException e) { // Error with transaction.
			e.printStackTrace();
			// Put something here like an error message.
			System.out.println("There was an error reading the file.");
		}
		return null;
	}
	
	public static void main(String[] arg) {
		//einlesen der beiden Dateien
		HashMap<Integer, String> words = textEinlesen(arg[0]);
		HashMap<Integer, String> dict  = wbEinlesen("woerter.txt");
		/*
		 * Alle Schlüssel aus arg[0] werden durch gegangen
		 * und überprüft ob sich das Wort auch im Wörterbuch befindet und wenn nicht
		 * wird der Value zu diesem Schlüssel ausgegeben. (also der dazugehörige String)
		 */
		Set<Integer> keys = words.keySet();
		for (Iterator<Integer> iterator = keys.iterator(); iterator.hasNext();) {
			Integer key = (Integer) iterator.next();
			if (!dict.containsKey(key)) {
				System.out.println(words.get(key));
			}
		}
	}
}
