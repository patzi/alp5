import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;

public class RemoteStackImpl implements RemoteStack  {
       int[] x = new int[10];
       int sp = 0;
	      public void push(int elem) { x[sp++] = elem; }
	      public void pop() { sp--; }
	      public int  top() { return x[sp-1]; }
	      public int[]dump(){ return x; }
	      public static void main(String[] arg) throws Exception {
	         RemoteStack s = new RemoteStackImpl();
		     RemoteStack stub = (RemoteStack)UnicastRemoteObject.exportObject(s, 0);
		     Registry registry = LocateRegistry.getRegistry();
		     registry.rebind("remote", stub);
	}
}
