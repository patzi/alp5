import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;

public class RMITest {
	public static void main(String[] arg) throws Exception {
		Map<String, RemoteStack> stacks = new HashMap<String, RemoteStack>();
		stacks.put("this", new RemoteStackImpl());
		Registry registry = LocateRegistry.getRegistry("xian.imp.fu-berlin.de");
		RemoteStack x = (RemoteStack) registry.lookup("remote");
		stacks.put("that", x);
		RemoteStack s1, s2;
		s1 = stacks.get("that");
		s2 = stacks.get("this");
		s1.push(3);
		s2.push(3);
		int[] there = s1.dump();
		int[] here = s2.dump();
		there[0] = 100;
		here[0] = 100;
		System.out.print("there: " + s1.top() + "   ");
		System.out.print("here:  " + s2.top() + "\n");
	}
}
