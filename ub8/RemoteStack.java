import java.rmi.*;
interface RemoteStack extends Remote {
	void push(int elem) throws RemoteException;
	void pop()  throws RemoteException;
	int  top()  throws RemoteException;
	int[]dump() throws RemoteException;
}
