import java.rmi.*;

interface duplikateEntfernen extends Remote {
	
	/*
	 * Der Funktion wird ein Array von Strings übergeben,
	 * welcher ohne doppelte Einträge zurückgegeben wird.
	 * Wenn nicht genauer Angegeben, wird das gesamte Array
	 * durchsucht und nicht casesensitiv verglichen.
	 */
	String[] entfDup(String[] array) throws RemoteException;

	// Array von start bis end durchsuchen
	String[] enfDup(String[] array,int start, int end) throws RemoteException;

	// Array von start bis end durchsuchen und auf Groß- und Kleinschreibung achten
	String[] enfDup(String[] array,int start, int end, boolean caseSen) throws RemoteException;

}
