import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Service implements Runnable{
	
	private ServerSocket serverSocket;
	private String dienst;
	private Socket clientSocket;
	private boolean betriebsbereit = false;
	
	Service(ServerSocket server, String dienst){
		this.serverSocket = server;
		this.dienst = dienst;
	}
	
	public void closeSocket() throws IOException{
		this.serverSocket.close();
	}
	
	public void run() {
		while (true) {
			try {
				clientSocket = serverSocket.accept();
				if (!betriebsbereit) {
					
				}
				Thread t1 = new Thread(new Bearbeiter(serverSocket, clientSocket));
				t1.run();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
