import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedList;

class Server {

	static LinkedList<String> ideen;
	static DatagramSocket serverSocket;
	static byte[] receiveData;
	static byte[] sendData;

	static void send(String msg, InetAddress IP, int port) throws Exception {
		sendData = new byte[1024];
		sendData = msg.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData,
				sendData.length, IP, port);
		serverSocket.send(sendPacket);
	}

	public Server() throws Exception {
		ideen = new LinkedList<String>();
		serverSocket = new DatagramSocket(9876);
		sendData = new byte[1024];
		receiveData = new byte[1024];
	}

	public Server(int port) throws Exception {
		ideen = new LinkedList<String>();
		serverSocket = new DatagramSocket(port);
		sendData = new byte[1024];
		receiveData = new byte[1024];
	}

	public static void main(String args[]) throws Exception {
		Server server = new Server();
		boolean ende = false;
		while (!ende) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			serverSocket.receive(receivePacket);
			String receivedString = new String(receivePacket.getData(), 0,receivePacket.getLength()-1);
			InetAddress IPAddress = receivePacket.getAddress();
			int port = receivePacket.getPort();
			System.out.println("RECEIVED: " + receivedString);
			if (receivedString.length() < 1) { 	//verhindert Fehler bei leerer Eingabe
				receivedString = " ";
			}
			String text = receivedString.substring(1);
			switch (receivedString.charAt(0)) {
			case 'i':
				ideen.addLast(text);
				String msg0 = "Idea added.";
				send(msg0, IPAddress, port);
				break;
			case 'l':
				String msg1 = new String();
				if (!ideen.isEmpty()) {
					msg1 = ideen.getLast();
				} else {
					msg1 = "Leer";
				}
				send(msg1, IPAddress, port);
				break;
			case 'a':
				String msg3 = null;
				if (!ideen.isEmpty()) {
					for (Iterator<String> iterator = ideen.iterator(); iterator.hasNext();) {
						if (msg3 == null) {
							msg3 = (String) iterator.next();
						} else {
							msg3 = msg3.concat((String) iterator.next());	
						}
					}
					System.out.println(msg3);
				} else {
					msg3 = "Leer";
				}
				send(msg3, IPAddress, port);
				break;
			case 'q':
				String msg4 = "Wird beendet.";
				send(msg4, IPAddress, port);
				System.out.println(msg4);
				serverSocket.close();
				ende = true;
				break;
			default:
				String msg5 = "usage: [i,a,l,q] <text>";
				send(msg5, IPAddress, port);
				break;
			}
		}
	} // end main
} // end class