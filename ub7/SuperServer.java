import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Map;


public class SuperServer {

	private static BufferedReader br;
	private static boolean ende = false;
	private static Map<Integer,Thread> dienste;	// Key-Value-Store für alle Dienste, Key = Port
	
	private static void dienstEinrichten(String dienstname, int port) throws IOException{
		ServerSocket serverSocket = new ServerSocket(port);
		Thread tnew = new Thread(new Service(serverSocket, dienstname));
		tnew.start();
		dienste.put(port, tnew);
	}
	
	private void dienstStarten(int port){
		Thread t = dienste.get(port);
	}
	
	// Methode zum lesen aus der config Datei
	private static int dateiEinlesen(String filename){
		File file = new File(filename);
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String inputline;
			while (br.ready()) {
				inputline = br.readLine();
				String [] result = inputline.split(" ");
				interpreter(result);
			}
		} catch (FileNotFoundException e) {
			return 1;
		} catch (IOException e) {
			e.printStackTrace();
			return 1;
		}
		return 0;
	}

	/*
	 *   shutdown beendet den Superserver "ordentlich".
	 *   open n service richtet einen Socket für Port n ein und legt das zugehörige Dienstprogramm fest.
	 *   close n schließt den Socket für Port n.
	 *   start n macht den dem Port n zugeordneten Dienst betriebsbereit.
	 *   stop n macht diesen Dienst vorübergehend unzugänglich.
	 */	
	private static void interpreter(String[] zeile) throws IOException{
		if (zeile[0].equals("shutdown")) {
			// TODO Ports schließen und alles Beenden
			ende = true;
		}
		if (zeile[0].equals("open")) {
			int port = Integer.parseInt(zeile[1]);	// Achtung Anzahl der Argumente werden nicht überprüft
			dienstEinrichten(zeile[2], port);
		}
	}
	
	public static void main(String[] args) throws IOException {
		if (args.length > 0) {
			dateiEinlesen(args[0]);
		} else {
			dateiEinlesen("SuperServer.config");
		}
		while(!ende){
			// Kommandozeile einlesen und Wörter trennen
			br = new BufferedReader(new InputStreamReader(System.in));
			String input = br.readLine();
			String [] result = input.split(" ");
			interpreter(result);
		}
	}
}
