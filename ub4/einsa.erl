%% Der koordinierende Prozess gewährt immer dem schnellsten Prozess
-module(einsa).
-export([init/0, test/1, coordinator/2]).
-export([dequeue/2, enqueue/1]).

init() ->
	Lock = spawn(einsa,coordinator,[none, []]),
 	spawn(einsa,test,[Lock]),
 	spawn(einsa,test,[Lock]),
 	spawn(einsa,test,[Lock]),
	timer:sleep(1000),
 	spawn(einsa,test,[Lock]),
 	spawn(einsa,test,[Lock]).

%% Koordinierender Prozess welcher sich in einer Schleife selbst aufruft
%% und sich dabei den Besitzer der Sperre und eine Liste der Wartenden selbstübergibt 
coordinator(LockOwner, List) ->
	NewLockOwner = 
	receive
		%% kritschen Bereich sperren
		{dolock, PID} ->
			if
				%% kein Prozess im kritischen Bereich
				LockOwner == none ->
					io:format("Coordinator(dolock): Lock granted~n", []),
					NewList = List,
					PID ! go,	%% erste_r in Warteschlange
					PID ;
				%% kritischer Bereich gesperrt
				true ->
					io:format("Coordinator: Lock denied~n", []),
					NewList = dequeue(List, PID),
					LockOwner
			end;
		%% krischen Bereich enrtsperren
		{unlock, PID} ->
			if
				%% richtiger Prozess entsperrt
				LockOwner == PID ->
					if 
						List =/= [] ->
							io:format("Coordinator(unlock): Lock granted~n", []),
							[Head|Tail] = List,
							NewList = enqueue(List),
							Head ! go,	%% erste_r in Warteschlange
							Head;
						true -> 
							NewList = List,
							none
					end;
				true ->
					NewList = List,
					LockOwner
			end
	end,
	coordinator(NewLockOwner, NewList).

%% Warteschlange (FIFO) für kritischen Bereich
%% einfügen :: [PID] + PID -> [PID]
dequeue(List, PID) ->
	NewList = List ++ [PID],
	NewList.

%% entfernen :: [PID] -> [PID] + PID
enqueue(List) ->
	[_|Tail] = List,
	NewList = Tail,
	NewList.

%% Damit es was zusehen gibt im Kritischen Bereich...
critical() ->
	io:format("~p: I'm in the critical section~n", [self()]),
	done.

test(Lock) ->
	%% quasi der Code aus der Aufgabenstellung
	Lock ! {dolock, self()},
	receive go -> true end,
	critical(),
	Lock ! {unlock, self()}.
	
