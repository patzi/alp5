-module(koordinator). %% Modulname und dateiname müssen übereinstimmen
-export([init/0, test/1, coordinator/1]). %% Namen und Anzahl der Parameter, der aufrufbaren Funktionen

init() ->
 	Lock = spawn(koordinator,coordinator,[none]),
 	spawn(koordinator,test,[Lock]),
 	spawn(koordinator,test,[Lock]),
 	spawn(koordinator,test,[Lock]),
	timer:sleep(1000),
 	spawn(koordinator,test,[Lock]),
 	spawn(koordinator,test,[Lock]),
	timer:sleep(1000),
 	spawn(koordinator,test,[Lock]),
 	spawn(koordinator,test,[Lock]).


coordinator(LockOwner) ->
	NewLockOwner =
	receive 
		{acquireLock, PID} ->
			if 
				LockOwner == none ->
					io:format("Coordinator: Lock granted~n", []),
					PID ! ok,
					PID;
				true ->
					io:format("Coordinator: Lock denied~n", []),
					PID ! no,
					LockOwner
			end;
		{releaseLock, PID} ->
			if
				LockOwner == PID ->
					PID ! ok,
					none;
				true ->
					PID ! fuckoff,
					LockOwner
			end
	end,
	coordinator(NewLockOwner).
	

critical() ->
	io:format("~p: I'm in the critical section~n", [self()]),
	done.

acquireLock(Lock) ->
	Lock ! {acquireLock, self()},
	receive 
		X ->
			X
	end.

releaseLock(Lock) ->
	Lock ! {releaseLock, self()},
	receive
		X ->
			X
	end.

test(Lock) ->
	Val = acquireLock(Lock),
	if 
		Val == ok ->
			critical(),
			releaseLock(Lock);
		true ->
			false
	end.
