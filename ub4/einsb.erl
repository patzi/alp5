%% Der koordinierende Prozess gewährt immer dem schnellsten Prozess
%% Zugriff und achtet nicht weiter auf Reinfolge der Anfragen oder verhunger und achtet nicht weiter auf Reinfolge der Anfragen oder verhungernn
-module(einsb).
-export([start/0, peer/2, test/1, coordinator/1]).


%% Koordinierender Prozess welcher sich in einer Schleife selbst aufruft
%% und sich dabei den Besitzer der Sperre selbstübergibt
coordinator(LockOwner) ->
	NewLockOwner = 
	receive
		%% kritschen Bereich sperren
		{dolock, PID} ->
			if
				%% kein Prozess im kritischen Bereich
				LockOwner == none ->
					io:format("Coordinator: Lock granted~n", []),
					PID ! go,
					PID;
				%% kritischer Bereich gesperrt
				true ->
					io:format("Coordinator: Lock denied~n", []),
					LockOwner
			end;
		%% krischen Bereich enrtsperren
		{unlock, PID} ->
			if
				%% richtiger Prozess entsperrt
				LockOwner == PID ->
					none;
				true ->
					LockOwner
			end
	end,
	coordinator(NewLockOwner).

%% Damit es was zusehen gibt im Kritischen Bereich...
critical() ->
	io:format("~p: I'm in the critical section~n", [self()]),
	done.

p(Lock) ->
	

v(Lock) ->

peer(Text, Lock) ->
    p(Lock),
    io:format("~w~n", [Text]),
    io:format("~w~n", [Text]),
    io:format("~w~n", [Text]),
    v(Lock),
    peer(Text, Lock).

start() -> 
    Lock = spawn(einsb, lock, .....),
    spawn(einsb, peer, [this, Lock]),
    spawn(einsb, peer, [andthat, Lock]). 
