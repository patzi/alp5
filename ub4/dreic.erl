-module(dreic).
-export([init/0, helper/0, station/1]).

init() ->
	Time = 0,
	Helper = spawn(dreic,helper,[]),
	spawn(dreic,station,[Helper, Time]),
	spawn(dreic,station,[Helper, Time]),
	spawn(dreic,station,[Helper, Time]),
	spawn(dreic,station,[Helper, Time]),
	spawn(dreic,station,[Helper, Time]),



helper(AlleStationen) ->
	receive
		{dolock, Time, PID}
			AlleStationen -- PID ! {anfrage, Time, PID};
		{erlaubnis, Time, Empfänger, PID}


 %% Damit es was zusehen gibt im Kritischen Bereich...
critical() ->
	io:format("~p: I'm in the critical section~n", [self()]),
	done.

station(Helper, Time) ->
	receive
		{anfrage, TimeAnfrage, PID} ->
			if
				not Bedarf and Time < TimeAnfrage or Time == TimeAnfrage and self < PID ->
					true,
				true ->
					Helper ! {erlaubnis, max(Time+1,TimeAnfrage),PID,self()}
			end
	end,

request(Time) ->
	NewTime = Time + 1,
	helper ! {dolock, NewTime, self()},
	receive go -> true end,
	critical(),
	done.
