-module(sema2).
-export([init/0, helper/2, client/1, client/2]).

init() ->
	Speicherplatz = 7,
	Semaphore = spawn(sema2,helper,[Speicherplatz, []]),
	spawn(sema2,client,[Semaphore]),
	spawn(sema2,client,[Semaphore]),
	spawn(sema2,client,[Semaphore]),
	timer:sleep(1000),
	spawn(sema2,client,[Semaphore]),
	spawn(sema2,client,[Semaphore]).


% Invariante: Count >= 0
helper(Count, List) ->
	NewCount =
	receive
		{p, N, ID} -> % N = Speicherbedarf
			if
				% TODO überprüfen ob die Anfrage den gesammten Speicher übersteigt
				N =< Count ->	% wenn genug Speicher vorhanden
					NewFree = Count - N, % neuer freier Speicher
					NewList = List,
					ID ! go,
					NewFree;
				true ->		% sonst
					NewList = List ++ {N, ID},
					Count	% Speicherbelegung bleibt gleich
			end;
		{v, N} ->	% Speicher wird wieder frei gegeben
			NewFree = Count + N,
			if
				List =/= [] ->	% es gibt wartende Prozesse
					Next = nextPlease(List, Count),
					if
					 	Next =/= nope ->
							NewList = List -- Next,
							{M, PID} = Next,
							NewNewFree = NewFree - M,
							PID ! go,
							NewNewFree;
						true ->
							NewList = List,
							NewFree
					end;
				true ->		% keine wartenden Prozesse
					NewList = List,
					NewFree
			end
	end,
	helper(NewCount, NewList).

% Geht List mit Tupel von Speicherbedarf und PID durch gibt nächst besten aus
% oder wenn kein passender Prozess gefunden werden kann wird "nope" ausgegeben
% nextPlease :: [{N,PID}] -> {N, PID}
nextPlease(List, Count) ->
	[Head|_] = List,
	{M,PID} = Head,
	if
		M =< Count ->
			{M, PID};
		true ->
			nope
	end.

%% Damit es was zusehen gibt im Kritischen Bereich...
critical() ->
	io:format("~p: I'm in the critical section~n", [self()]),
	done.

client(Semaphore) ->
	% remainder of code
	Semaphore ! {p, 3, self()},
	receive go -> true end,
	critical(),
	Semaphore ! {v, 3},
	done.

client(Semaphore, N) ->
	% remainder of code
	Semaphore ! {p, N, self()},
	receive go -> true end,
	critical(),
	Semaphore ! {v, N},
	done.

