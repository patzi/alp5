-module(zweib).
-export([init/0]).

init() ->
	Display = spawn(zweib,lock,[false]),
	Store = ets:new(store,[set, named_table]),
	spawn(zweib,repository,[Display, Store]).

lock(Locked) ->
	NewLock =
	receive
		{dolock, PID}	->
			if
				Locked =/= true ->
					true;
				true ->
					false
			end;
		{unlock}	->
			false
	end,
	lock(NewLock).

repository(D, Store) ->
	done.

%deposit :: [xs] + x -> [xs:x]
%deposit gibt ticket zurück
deposit(Store, Data) ->
	Ticket = crypto:rand_bytes(1),
	ets:insert(Store,{Ticket, Data}),
	% LOCK
	% printf("\n");
	% UNLOCK
	Ticket.

% pickup :: [xs:x] -> x
% pickup entfernt und gibt x aus
% wenn Ticket nicht vorhanden dann wird [] zurück gegeben
pickup(Store, Ticket) ->
	Daten = ets:lookup(Store, Ticket),
	ets:delete(Store, Ticket),
	Daten.
