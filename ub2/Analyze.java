
// usage:  java Analyze host1 text1.txt host2 text2.txt dictionary.txt
// e.g.    java Analyze xian.imp.fu-berlin.de obituary1.txt \
//                      wuhan.imp.fu-berlin.de obituary2.txt dictionary.txt
// files Check.class and *.txt must be present in ~/testdir

import java.io.*;
import java.util.*;

public class Analyze {
       static Histogram hist = new Histogram();

public static void main(String[] arg) throws Exception  {
	Process p1 = null;
	Process p2 = null;
	if (arg.length > 0){
		
       String host1 = arg[0];  String text1 = arg[1];
       String host2 = arg[2];  String text2 = arg[3];
       String dictionary = arg[4];

       p1 = Runtime.getRuntime().exec(
               "ssh " + host1 + " cd testdir; java Check " + text1 + " " + dictionary);
       p2 = Runtime.getRuntime().exec(
               "ssh " + host2 + " cd testdir; java Check " + text2 + " " + dictionary);
	}

	if (arg.length > 0){
       BufferedReader in1 = new BufferedReader(
                            new InputStreamReader(p1.getInputStream()));
       BufferedReader in2 = new BufferedReader(
                            new InputStreamReader(p2.getInputStream()));
       Thread t1 = new Reader(in1); t1.start(); 
       Thread t2 = new Reader(in2); t2.start(); 
       t1.join();
       t2.join();
	} else {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
 		Thread t1 = new Reader(in); t1.start(); 
	        t1.join();
	}
       print();
       System.exit(0);
}
       static void print() { // in lexicographical order
              final Map<String,Integer> map = new TreeMap<String,Integer>();
              hist.iterate(new Bag.Action<String>() {			       public void act(String s, int i) {
                                               map.put(s, i); } } );
              System.out.println();
              System.out.println(map);

       }
       static class Reader extends Thread {       
	      Reader(BufferedReader in) { this.in = in; }   
	      BufferedReader in;   
       public void run()  {
              String word = null;
              for(;;) { try { word = in.readLine(); }
                        catch(IOException e) {
			      System.out.println("input channel broken - exit.");
                              System.exit(-1); } 
                        if(word==null) return;
                        else { hist.add(word); System.out.print(word + " "); }  }	
        }
        } // end Reader

} // end Analyze

