public interface Bag<T> { // model: multiset of elements of type T
void add(T item);  // add item to multiset
void iterate(Action<T> action); // internal iterator:
                   // execute action.act(t,n) for all different elements t,
                   // where n is the occurrence count for each t
interface Action<X> { void act(X x, int i); }
}